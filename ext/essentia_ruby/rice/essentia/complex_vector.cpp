#include <iostream>
#include <rice/Constructor.hpp>

#include "exception.hpp"
#include "modules.hpp"

namespace Rice
  {
  namespace Essentia
  {
    static Rice::Data_Type<std::vector<std::complex<essentia::Real> > > rb_cStdVectorEssentiaComplex;

    void
    install_vector_complex()
    {
      RUBY_TRY
      {
        rb_cStdVectorEssentiaComplex =
          define_class_under<std::vector<std::complex<essentia::Real> > >(essentia_module(), "ComplexVector")
          .add_handler<essentia::EssentiaException>(handle_essentia_exception)
          ;
      }
      RUBY_CATCH
    }

  }
}
