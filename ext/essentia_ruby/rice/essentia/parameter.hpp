#if !defined(_RICE_ESSENTIA_PARAMETER_HPP_)
# define _RICE_ESSENTIA_PARAMETER_HPP_

namespace Rice
{
  namespace Essentia
  {
    void install_parameters();
  }
}

#endif /* !defined(_RICE_ESSENTIA_PARAMETER_HPP_) */
