#include "rice/Data_Type.hpp"
#include "rice/Data_Object.hpp"
#include "rice/Constructor.hpp"
#include "rice/Array.hpp"
#include "rice/Enum.hpp"
#include "rice/to_from_ruby.hpp"
#include "rice/ruby_try_catch.hpp"

#include "types.hpp"
#include "modules.hpp"
#include "exception.hpp"
#include "to_from_ruby.hpp"
#include "parameter.hpp"
#include "param_type.hpp"
#include "stereo_sample_proxy.hpp"

namespace Rice {
  namespace Essentia {

    /*
     * we need to define a template wrapper to accomodate for all the
     * different constructors
     */
    template <typename T>
    essentia::Parameter &
    wrap_constructor(const T & t)
    {
      return *(new essentia::Parameter(t));
    }

    static Rice::Data_Type<essentia::Parameter> rb_cParameter;

    void
    install_parameters()
    {
      RUBY_TRY
      {
        void install_parameter_type();

        install_parameter_type();

        rb_cParameter =
          define_class_under<essentia::Parameter>(essentia_module(), "Parameter")
          .define_constructor(Constructor<essentia::Parameter, essentia::Parameter::ParamType&>())
          .add_handler<essentia::EssentiaException>(handle_essentia_exception)
          .define_singleton_method("new_string", &wrap_constructor<std::string>, (Arg("string")))
          .define_singleton_method("new_real", &wrap_constructor<essentia::Real>, (Arg("real")))
          .define_singleton_method("new_bool", &wrap_constructor<bool>, (Arg("bool")))
          .define_singleton_method("new_int", &wrap_constructor<int>, (Arg("int")))
          .define_singleton_method("new_uint", &wrap_constructor<uint>, (Arg("uint")))
          .define_singleton_method("new_stereo_sample", &wrap_constructor<essentia::StereoSample>, (Arg("stereo_sample")))
          .define_singleton_method("new_vector_real", &wrap_constructor<std::vector<essentia::Real> >, (Arg("vector_real")))
          .define_singleton_method("new_vector_string", &wrap_constructor<std::vector<std::string> >, (Arg("vector_string")))
          .define_singleton_method("new_vector_bool", &wrap_constructor<std::vector<bool> >, (Arg("vector_bool")))
          .define_singleton_method("new_vector_int", &wrap_constructor<std::vector<int> >, (Arg("vector_int")))
          .define_singleton_method("new_vector_stereo_sample", &wrap_constructor<std::vector<essentia::StereoSample> >, (Arg("vector_stereo_sample")))
          .define_method("clear", &essentia::Parameter::clear)
          .define_method("assign", &essentia::Parameter::operator=)
          .define_method("==", &essentia::Parameter::operator==)
          .define_method("!=", &essentia::Parameter::operator!=)
          .define_method("type", &essentia::Parameter::type)
          .define_method("configured?", &essentia::Parameter::isConfigured)
          .define_method("to_s", &essentia::Parameter::toString, (Arg("precision") = 12))
          .define_method("downcase", &essentia::Parameter::toLower)
          .define_method("to_boolean", &essentia::Parameter::toBool)
          .define_method("to_double", &essentia::Parameter::toDouble)
          .define_method("to_f", &essentia::Parameter::toFloat)
          .define_method("to_stereo_sample", &essentia::Parameter::toStereoSample)
          .define_method("to_i", &essentia::Parameter::toInt)
          .define_method("to_real", &essentia::Parameter::toReal)
          .define_method("to_vector_real", &essentia::Parameter::toVectorReal)
          .define_method("to_vector_string", &essentia::Parameter::toVectorString)
          .define_method("to_vector_int", &essentia::Parameter::toVectorInt)
          .define_method("to_vector_boolean", &essentia::Parameter::toVectorBool)
          .define_method("to_vector_stereo_sample", &essentia::Parameter::toVectorStereoSample)
          .define_method("to_vector_vector_real", &essentia::Parameter::toVectorVectorReal)
          .define_method("to_vector_vector_string", &essentia::Parameter::toVectorVectorString)
          .define_method("to_vector_vector_stereo_sample", &essentia::Parameter::toVectorVectorStereoSample)
          .define_method("to_vector_matrix_real", &essentia::Parameter::toVectorMatrixReal)
          .define_method("to_map_vector_real", &essentia::Parameter::toMapVectorReal)
          .define_method("to_map_vector_string", &essentia::Parameter::toMapVectorString)
          .define_method("to_map_real", &essentia::Parameter::toMapReal)
          .define_method("to_matrix_real", &essentia::Parameter::toMatrixReal)
          ;
      }
      RUBY_CATCH
    }

  } // namespace Essentia
} // namespace Rice
