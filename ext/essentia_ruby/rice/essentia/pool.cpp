#include "rice/Data_Type.hpp"
#include "rice/Constructor.hpp"

#include <essentia/pool.h>

#include "exception.hpp"
#include "modules.hpp"

namespace Rice
{
  namespace Essentia
  {
    static Rice::Data_Type<essentia::Pool> rb_cPool;
    
    typedef void (essentia::Pool::*add_real)(const std::string &, const essentia::Real &, bool);
    typedef void (essentia::Pool::*add_vector_real)(const std::string &, const std::vector<essentia::Real> &, bool);
    typedef void (essentia::Pool::*add_string)(const std::string &, const std::string &, bool);
    typedef void (essentia::Pool::*add_vector_string)(const std::string &, const std::vector<std::string> &, bool);
    typedef void (essentia::Pool::*add_stereo_sample)(const std::string &, const essentia::StereoSample &, bool);

    const essentia::Real &
    wrap_real_value(Rice::Object o, Rice::Object k)
    {
      Rice::Data_Object<essentia::Pool> pool(o);
      std::string key = from_ruby<std::string>(k);

      return pool->value<essentia::Real>(key);
    }

    void
    wrap_add_real(Rice::Object o, Rice::Object k, Rice::Object v, Rice::Object valid)
    {
      Rice::Data_Object<essentia::Pool> pool(o);
      std::string key = from_ruby<std::string>(k);
      essentia::Real val = from_ruby<essentia::Real>(v);
      bool validity = from_ruby<bool>(valid);

      pool->add(key, val, validity);
    }

    void
    install_pools()
    {
      RUBY_TRY
      {
        rb_cPool =
          define_class_under<essentia::Pool>(essentia_module(), "Pool")
          .define_constructor(Constructor<essentia::Pool>())
          .add_handler<essentia::EssentiaException>(handle_essentia_exception)
          .define_method<std::vector<std::string> (essentia::Pool::*)() const>("descriptor_names", &essentia::Pool::descriptorNames)
          .define_method<bool (essentia::Pool::*)(const std::string &) const>("contains_real", &essentia::Pool::contains<essentia::Real>, (Arg("name")))
          .define_method<bool (essentia::Pool::*)(const std::string &) const>("contains_real_vector", &essentia::Pool::contains<std::vector<essentia::Real> >, (Arg("name")))
//        .define_method("add_real", add_real(&essentia::Pool::add), (Arg("key"), Arg("value"), Arg("validityCheck") = false))
          .define_method("add_real", &wrap_add_real, (Arg("key"), Arg("value"), Arg("validityCheck") = false))
          .define_method("add_vector_real", add_vector_real(&essentia::Pool::add), (Arg("key"), Arg("value"), Arg("validityCheck") = false))
          .define_method("add_string", add_string(&essentia::Pool::add), (Arg("key"), Arg("value"), Arg("validityCheck") = false))
          .define_method("add_vector_string", add_vector_string(&essentia::Pool::add), (Arg("key"), Arg("value"), Arg("validityCheck") = false))
          .define_method("add_stereo_sample", add_stereo_sample(&essentia::Pool::add), (Arg("key"), Arg("value"), Arg("validityCheck") = false))
//        .define_method("real_value", &essentia::Pool::value<essentia::Real>)
          .define_method("real_value", &wrap_real_value)
          ;
      }
      RUBY_CATCH
    }

  } // namespace Essentia
} // namespace Rice
