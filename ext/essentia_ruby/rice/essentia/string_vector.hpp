#if !defined(_RICE_ESSENTIA_STRING_VECTOR_HPP_)
#  define _RICE_ESSENTIA_STRING_VECTOR_HPP_

namespace Rice {
  namespace Essentia {
    void install_vector_string();
  }
}

#endif /* !defined(_RICE_ESSENTIA_STRING_VECTOR_HPP_) */
