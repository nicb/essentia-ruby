#include <iostream>
#include <rice/Constructor.hpp>

#include "exception.hpp"
#include "modules.hpp"

namespace Rice
  {
  namespace Essentia
  {
    static Rice::Data_Type<std::vector<std::string> > rb_cStdVectorEssentiaString;

    void
    install_vector_string()
    {
      RUBY_TRY
      {
        rb_cStdVectorEssentiaString =
          define_class_under<std::vector<std::string> >(essentia_module(), "StringVector")
          .add_handler<essentia::EssentiaException>(handle_essentia_exception)
          ;
      }
      RUBY_CATCH
    }

  }
}
