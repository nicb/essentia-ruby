#include "rice/Constructor.hpp"
#include "rice/to_from_ruby.hpp"

#include <essentia/algorithmfactory.h>
#include "exception.hpp"
#include "modules.hpp"
#include "algorithm_factory.hpp"

template <>
Rice::Object
to_ruby<essentia::standard::AlgorithmFactory>(essentia::standard::AlgorithmFactory const &af)
{
  essentia::standard::AlgorithmFactory::init();
  return Rice::Data_Object<essentia::standard::AlgorithmFactory>(&(essentia::standard::AlgorithmFactory::instance()));
}

template <>
Rice::Object
to_ruby<essentia::streaming::AlgorithmFactory>(essentia::streaming::AlgorithmFactory const &af)
{
  essentia::streaming::AlgorithmFactory::init();
  return Rice::Data_Object<essentia::streaming::AlgorithmFactory>(&(essentia::streaming::AlgorithmFactory::instance()));
}

namespace Rice
{
  namespace Essentia
  {
    namespace Standard
    {
      static essentia::standard::Algorithm *
      wrap_esaf_create_standard0(const std::string &id)
      {
        return essentia::standard::AlgorithmFactory::create(id);
      }

      static essentia::standard::Algorithm *
      wrap_esaf_create_standard1(const std::string &id, const std::string &key1, const essentia::Parameter &par1)
      {
        return essentia::standard::AlgorithmFactory::create(id, key1, par1);
      }

      static essentia::standard::Algorithm *
      wrap_esaf_create_standard2(const std::string &id, const std::string &key1, const essentia::Parameter &par1, const std::string &key2, const essentia::Parameter &par2)
      {
        return essentia::standard::AlgorithmFactory::create(id, key1, par1, key2, par2);
      }

      static essentia::standard::Algorithm *
      wrap_esaf_create_standard3(const std::string &id, const std::string &key1, const essentia::Parameter &par1, const std::string &key2, const essentia::Parameter &par2, const std::string &key3, const essentia::Parameter &par3)
      {
        return essentia::standard::AlgorithmFactory::create(id, key1, par1, key2, par2, key3, par3);
      }

      static Rice::Data_Type<essentia::standard::AlgorithmFactory> rb_cStandardAlgorithmFactory;
      static Rice::Data_Type<AlgorithmFactoryProxy> rb_cStandardAlgorithmFactoryProxy;

      void
      install_algorithm_factory()
      {
         RUBY_TRY
         {
           rb_cStandardAlgorithmFactory =
              define_class_under<essentia::standard::AlgorithmFactory>(essentia_standard_module(), "AlgorithmFactory__")
              .define_singleton_method("instance", &essentia::standard::AlgorithmFactory::instance)
              .define_singleton_method("init", &essentia::standard::AlgorithmFactory::init)
              .define_singleton_method("shutdown", &essentia::standard::AlgorithmFactory::shutdown)
              .define_singleton_method("keys", &essentia::standard::AlgorithmFactory::keys)
              .define_singleton_method("create0", &wrap_esaf_create_standard0, (Arg("id")))
              .define_singleton_method("create1", &wrap_esaf_create_standard1, (Arg("id"), Arg("key1"), Arg("par1")))
              .define_singleton_method("create2", &wrap_esaf_create_standard2, (Arg("id"), Arg("key1"), Arg("par1"), Arg("key2"), Arg("par2")))
              .define_singleton_method("create3", &wrap_esaf_create_standard3, (Arg("id"), Arg("key1"), Arg("par1"), Arg("key2"), Arg("par2"), Arg("key3"), Arg("par3")))
             ;

           rb_cStandardAlgorithmFactoryProxy =
             define_class_under<AlgorithmFactoryProxy, essentia::standard::AlgorithmFactory>(essentia_standard_module(), "AlgorithmFactory")
             .define_director<AlgorithmFactoryProxy>()
             .define_constructor(Rice::Constructor<AlgorithmFactoryProxy, Rice::Object>())
             ;
         }
         RUBY_CATCH
      }
    }

    namespace Streaming
    {

      static essentia::streaming::Algorithm *
      wrap_esaf_create_streaming0(const std::string &id)
      {
        return essentia::streaming::AlgorithmFactory::create(id);
      }

      static essentia::streaming::Algorithm *
      wrap_esaf_create_streaming1(const std::string &id, const std::string &key1, const essentia::Parameter &par1)
      {
        return essentia::streaming::AlgorithmFactory::create(id, key1, par1);
      }

      static essentia::streaming::Algorithm *
      wrap_esaf_create_streaming2(const std::string &id, const std::string &key1, const essentia::Parameter &par1, const std::string &key2, const essentia::Parameter &par2)
      {
        return essentia::streaming::AlgorithmFactory::create(id, key1, par1, key2, par2);
      }

      static essentia::streaming::Algorithm *
      wrap_esaf_create_streaming3(const std::string &id, const std::string &key1, const essentia::Parameter &par1, const std::string &key2, const essentia::Parameter &par2, const std::string &key3, const essentia::Parameter &par3)
      {
        return essentia::streaming::AlgorithmFactory::create(id, key1, par1, key2, par2, key3, par3);
      }


      static Rice::Data_Type<essentia::streaming::AlgorithmFactory> rb_cStreamingAlgorithmFactory;
      static Rice::Data_Type<AlgorithmFactoryProxy> rb_cStreamingAlgorithmFactoryProxy;

      void
      install_algorithm_factory()
      {
         RUBY_TRY
         {
           rb_cStreamingAlgorithmFactory =
              define_class_under<essentia::streaming::AlgorithmFactory>(essentia_streaming_module(), "AlgorithmFactory__")
              .define_singleton_method("instance", &essentia::streaming::AlgorithmFactory::instance)
              .define_singleton_method("init", &essentia::streaming::AlgorithmFactory::init)
              .define_singleton_method("shutdown", &essentia::streaming::AlgorithmFactory::shutdown)
              .define_singleton_method("keys", &essentia::streaming::AlgorithmFactory::keys)
              .define_singleton_method("create0", &wrap_esaf_create_streaming0, (Arg("id")))
              .define_singleton_method("create1", &wrap_esaf_create_streaming1, (Arg("id"), Arg("key1"), Arg("par1")))
              .define_singleton_method("create2", &wrap_esaf_create_streaming2, (Arg("id"), Arg("key1"), Arg("par1"), Arg("key2"), Arg("par2")))
              .define_singleton_method("create3", &wrap_esaf_create_streaming3, (Arg("id"), Arg("key1"), Arg("par1"), Arg("key2"), Arg("par2"), Arg("key3"), Arg("par3")))
             ;

           rb_cStreamingAlgorithmFactoryProxy =
             define_class_under<AlgorithmFactoryProxy, essentia::streaming::AlgorithmFactory>(essentia_streaming_module(), "AlgorithmFactory")
             .define_director<AlgorithmFactoryProxy>()
             .define_constructor(Rice::Constructor<AlgorithmFactoryProxy, Rice::Object>())
             ;
         }
         RUBY_CATCH
      }
    }

  }
}
