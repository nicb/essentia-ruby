#include <iostream>

#include "to_from_ruby.hpp"
#include "exception.hpp"

  // void * specialization
  // a real specialization to a <void *> type cannot be done, so we
  // hack it by inventing an empty struct void_masker with which we do
  // specialize the pointer
  // The <void *> function pointer should be cast to that struct
  // in the calls that go from and to ruby
  //
template <>
Rice::void_masker *
from_ruby<Rice::void_masker *>(Rice::Object x)
{
  return (Rice::void_masker *) x.value();
}

template <>
Rice::Object
to_ruby<Rice::void_masker *>(Rice::void_masker *const &r)
{
  Rice::Object result((VALUE) r);

  return result;
}

//
// standard types
//
template <>
essentia::Real *
from_ruby<essentia::Real *>(Rice::Object o)
{
   essentia::Real *r = new essentia::Real;
   double d;
   Rice::protect(Rice::detail::num2dbl, o, &d);
   *r = (essentia::Real) d;
   return r;
}

template <>
std::complex<essentia::Real> *
from_ruby<std::complex<essentia::Real> *>(Rice::Object o)
{
  std::complex<essentia::Real> *c = 0;
  double r, i;

  if (o.rb_type() == T_COMPLEX)
  {
    Rice::protect(Rice::detail::num2dbl, o.call("real"), &r);
    Rice::protect(Rice::detail::num2dbl, o.call("imag"), &i);
    c = new std::complex<essentia::Real>(r, i);
  }
  else
    Rice::Essentia::throw_invalid_from_ruby_argument_exception<std::complex<essentia::Real> *>(o);

  return c;
}

template <>
std::complex<essentia::Real>
from_ruby<std::complex<essentia::Real> >(Rice::Object o)
{
  return *(from_ruby<std::complex<essentia::Real> *>(o));
}

template <>
Rice::Object
to_ruby<std::complex<essentia::Real> >(std::complex<essentia::Real> const &c)
{
  Rice::Object result(rb_complex_raw(Rice::detail::dbl2num(c.real()), Rice::detail::dbl2num(c.imag())));

  return result;
}

template <>
bool *
from_ruby<bool *>(Rice::Object o)
{
  bool *bp = new bool;
  *bp = from_ruby<bool>(o);
  return bp;
}

template <>
int *
from_ruby<int *>(Rice::Object i)
{
  int *p = new int;
  *p = from_ruby<int>(i);
  return p;
}

template <>
double *
from_ruby<double *>(Rice::Object d)
{
  double *p = new double;
  Rice::protect(Rice::detail::num2dbl, d, p);
  return p;
}

template <>
unsigned int *
from_ruby<unsigned int *>(Rice::Object u)
{
  unsigned int *p = new unsigned int;

  *p = from_ruby<unsigned int>(u);
  return p;
}

template <>
Rice::Essentia::StereoSampleProxy *
from_ruby<Rice::Essentia::StereoSampleProxy *>(Rice::Object o)
{
  Rice::Essentia::StereoSampleProxy *result = 0;

  if (o.rb_type() == T_ARRAY)
  {
    std::vector<essentia::Real> *arg = from_ruby<std::vector<essentia::Real> *>(o);
    result = new Rice::Essentia::StereoSampleProxy(arg);
  }
  else if (o.rb_type() == T_DATA)
    result = Rice::Data_Type<Rice::Essentia::StereoSampleProxy>::from_ruby(o);
  else
    result = new Rice::Essentia::StereoSampleProxy();

  return result;
}

template <>
essentia::StereoSample *
from_ruby<essentia::StereoSample *>(Rice::Object o)
{
  essentia::StereoSample *result = 0;

  if (o.rb_type() == T_ARRAY)
  {
    std::vector<essentia::Real> *arg = from_ruby<std::vector<essentia::Real> *>(o);
    result = new essentia::StereoSample();
    if (arg->size() >= 2)
      result->second = (*arg)[1];
    if (arg->size() >= 1)
      result->first = (*arg)[0];
  }
  else if (o.rb_type() == T_DATA)
    result = Rice::Data_Type<essentia::StereoSample>::from_ruby(o);
  else
    result = new essentia::StereoSample();

  return result;
}

template <>
essentia::StereoSample
from_ruby<essentia::StereoSample>(Rice::Object o)
{
  return *(from_ruby<essentia::StereoSample *>(o));
}

template <>
Rice::Object
to_ruby<essentia::StereoSample>(essentia::StereoSample const &ss)
{
  Rice::Array a;

  a.push(ss.first);
  a.push(ss.second);

  return a;
}

//
// vector and vector-of-vectors to/from ruby conversions
//
//
// detail conversion first

namespace Rice
{
  namespace detail
  {
    template <typename T>
    typename from_ruby_vector_<T>::Retval_T *
    from_ruby_vector_<T>::convert(Rice::Object x)
    {
      Retval_T *result = 0;

      if (rb_type(x.value()) == T_ARRAY)
      {
        Rice::Array a(x);
        result = new Retval_T;
        result->reserve(a.size());
        for (Rice::Array::iterator it = a.begin(); it != a.end(); ++it)
          result->push_back(from_ruby<T>(*it));
      }
      else
        Essentia::throw_invalid_from_ruby_argument_exception<T>(x);

      return result;
    }

    template <typename T>
    Rice::Object
    to_ruby_vector_<T>::convert(std::vector<T> const &x)
    {
      Rice::Object null_object;

      if (Data_Type<std::vector<T> >::is_bound())
        return Rice::Array(x.begin(), x.end());
      else
        Essentia::throw_invalid_to_ruby_argument_exception<std::vector<T> >();

      return null_object; // should never reach here - just to keep the compiler happy
    }

    template <typename T>
    typename from_ruby_vector_vector_<T>::Retval_T *
    from_ruby_vector_vector_<T>::convert(Rice::Object x)
    {
      Retval_T *result = 0;

      if (rb_type(x.value()) == T_ARRAY)
      {
        Rice::Array a(x);
        result = new Retval_T;
        result->reserve(a.size());
        for (Rice::Array::iterator it = a.begin(); it != a.end(); ++it)
          result->push_back(*(from_ruby<std::vector<T > *>(*it)));
      }
      else
        Essentia::throw_invalid_from_ruby_argument_exception<T>(x);

      return result;
    }

    template <typename T>
    Rice::Object
    to_ruby_vector_vector_<T>::convert(std::vector<std::vector<T> > const &x)
    {
      Rice::Object null_object;

      if (Data_Type<std::vector<std::vector<T> > >::is_bound())
      {
        Rice::Array a;

        for (typename std::vector<std::vector<T> >::const_iterator it = x.begin(); it != x.end(); ++it)
          a.push(to_ruby<std::vector<T> >(*it));

        return a;
      }
      else
        Essentia::throw_invalid_to_ruby_argument_exception<std::vector<std::vector<T> > >();

      return null_object; // should never reach here - just to keep the compiler happy
    }

  } // namespace detail
} // namespace Rice

// actual conversions

template <>
std::vector<essentia::Real> *
from_ruby<std::vector<essentia::Real> *>(Rice::Object x)
{
  return Rice::detail::from_ruby_vector_<essentia::Real>::convert(x);
}

template <>
Rice::Object
to_ruby<std::vector<essentia::Real> >(std::vector<essentia::Real> const &x)
{
  return Rice::detail::to_ruby_vector_<essentia::Real>::convert(x);
}

template <>
std::vector<std::vector<essentia::Real> >*
from_ruby<std::vector<std::vector<essentia::Real> > *>(Rice::Object x)
{
  return Rice::detail::from_ruby_vector_vector_<essentia::Real>::convert(x);
}

template <>
Rice::Object
to_ruby<std::vector<std::vector<essentia::Real> > >(std::vector<std::vector<essentia::Real> >const &x)
{
  return Rice::detail::to_ruby_vector_vector_<essentia::Real>::convert(x);
}

template <>
std::vector<std::complex<essentia::Real> > *
from_ruby<std::vector<std::complex<essentia::Real> > *>(Rice::Object o)
{
  return Rice::detail::from_ruby_vector_<std::complex<essentia::Real> >::convert(o);
}

template <>
Rice::Object
to_ruby<std::vector<std::complex<essentia::Real> > >(std::vector<std::complex<essentia::Real> > const &c)
{
  return Rice::detail::to_ruby_vector_<std::complex<essentia::Real > >::convert(c);
}

template <>
std::vector<std::string> *
from_ruby<std::vector<std::string> *>(Rice::Object x)
{
  return Rice::detail::from_ruby_vector_<std::string>::convert(x);
}

template <>
Rice::Object
to_ruby<std::vector<std::string> >(std::vector<std::string> const &x)
{
  return Rice::detail::to_ruby_vector_<std::string>::convert(x);
}

template <>
std::vector<bool> *
from_ruby<std::vector<bool> *>(Rice::Object x)
{
  return Rice::detail::from_ruby_vector_<bool>::convert(x);
}

template <>
Rice::Object
to_ruby<std::vector<bool> >(std::vector<bool> const &x)
{
  return Rice::detail::to_ruby_vector_<bool>::convert(x);
}

template <>
std::vector<int> *
from_ruby<std::vector<int> *>(Rice::Object x)
{
  return Rice::detail::from_ruby_vector_<int>::convert(x);
}

template <>
Rice::Object
to_ruby<std::vector<int> >(std::vector<int> const &x)
{
  return Rice::detail::to_ruby_vector_<int>::convert(x);
}

template <>
std::vector<Rice::Essentia::StereoSampleProxy> *
from_ruby<std::vector<Rice::Essentia::StereoSampleProxy> *>(Rice::Object x)
{
  return Rice::detail::from_ruby_vector_<Rice::Essentia::StereoSampleProxy>::convert(x);
}

template <>
std::vector<essentia::StereoSample> *
from_ruby<std::vector<essentia::StereoSample> *>(Rice::Object x)
{
  return Rice::detail::from_ruby_vector_<essentia::StereoSample>::convert(x);
}
