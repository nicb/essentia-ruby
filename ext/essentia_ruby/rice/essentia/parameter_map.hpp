#if !defined(_RICE_ESSENTIA_PARAMETER_MAP_HPP_)
#  define _RICE_ESSENTIA_PARAMETER_MAP_HPP_

#include "essentia/parameter.h"
#include "parameter.hpp"

namespace Rice
{
  namespace Essentia
  {

    class ParameterMapProxy : public essentia::ParameterMap
    {
      public:

        static Rice::Object create(Rice::Object);

        void add_(const std::string &key, const essentia::Parameter &v) { this->add(key, v); }

        size_t size_() const { return this->size(); }

        bool is_empty() const { return (this->size_() == 0); }

        std::vector<std::string> keys_() { return this->keys(); }

        essentia::Parameter & sqb(std::string k) { return (*this)[k]; }

    };

    void install_parameter_maps();
  }
}

#endif /* !defined(_RICE_ESSENTIA_PARAMETER_MAP_HPP_) */
