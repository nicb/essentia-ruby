#if !defined(_essentia_algorithm_factory_hpp_)
# define _essentia_algorithm_factory_hpp_

#include "rice/Director.hpp"

#include <essentia/algorithmfactory.h>

template <> Rice::Object to_ruby<essentia::standard::AlgorithmFactory>(essentia::standard::AlgorithmFactory const &);

namespace Rice
{
  namespace Essentia
  {
    namespace Standard
    {
      class AlgorithmFactoryProxy : public essentia::standard::AlgorithmFactory, Rice::Director
      {
      public:
        AlgorithmFactoryProxy(Rice::Object self) : essentia::standard::AlgorithmFactory(), Rice::Director(self) {}
      };

      void install_algorithm_factory();
    }
  }
}

namespace Rice
{
  namespace Essentia
  {
    namespace Streaming
    {
      class AlgorithmFactoryProxy : public essentia::streaming::AlgorithmFactory, Rice::Director
      {
      public:
        AlgorithmFactoryProxy(Rice::Object self) : essentia::streaming::AlgorithmFactory(), Rice::Director(self) {}
      };

      void install_algorithm_factory();
    }
  }
}

#endif /* !defined(_essentia_algorithm_factory_hpp_) */
