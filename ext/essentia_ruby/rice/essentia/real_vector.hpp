#if !defined(_RICE_ESSENTIA_REAL_VECTOR_HPP_)
#  define _RICE_ESSENTIA_REAL_VECTOR_HPP_

#include <essentia/types.h>

namespace Rice {
  namespace Essentia {
    void install_vector_real();
    void install_vector_vector_real();
  }
}

#endif /* !defined(_RICE_ESSENTIA_REAL_VECTOR_HPP_) */
