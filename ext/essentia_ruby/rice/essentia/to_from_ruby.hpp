#if !defined(_RICE_ESSENTIA_TO_FROM_RUBY_HPP_)
# define _RICE_ESSENTIA_TO_FROM_RUBY_HPP_

#include <essentia/types.h>

// This is the file where we keep all the from_ruby/to_ruby specializations
// which happen within Essentia. It was picked up directly from
//
// https://github.com/jasonroelofs/rice/wiki/FAQ
//
// and then further extended and modified.
//
// Not an official example; there may be a better way. Tested in rice 1.4.0.
//
#include <rice/Object.hpp>
#include <rice/Hash.hpp>
#include <rice/to_from_ruby.hpp>

#include "stereo_sample_proxy.hpp"
#include "types.hpp"

namespace Rice
{
  //
  // this is needed to be able to dereference void pointers
  struct void_masker {};

} // namespace Rice


// function prototypes

template<> Rice::void_masker *from_ruby<Rice::void_masker *>(Rice::Object);
template<> Rice::Object to_ruby<Rice::void_masker *>(Rice::void_masker *const &);

template<> essentia::Real *from_ruby<essentia::Real *>(Rice::Object);
template<> essentia::AudioSample *from_ruby<essentia::AudioSample *>(Rice::Object);
template<> bool *from_ruby<bool *>(Rice::Object);
template<> int *from_ruby<int *>(Rice::Object);
template<> double *from_ruby<double *>(Rice::Object);
template<> uint *from_ruby<uint *>(Rice::Object);
template <> std::complex<essentia::Real> *from_ruby<std::complex<essentia::Real> *>(Rice::Object);
template <> std::complex<essentia::Real> from_ruby<std::complex<essentia::Real> >(Rice::Object);
template <> Rice::Object to_ruby<std::complex<essentia::Real> >(std::complex<essentia::Real> const &);

template<> Rice::Essentia::StereoSampleProxy *from_ruby<Rice::Essentia::StereoSampleProxy *>(Rice::Object);
template<> essentia::StereoSample * from_ruby<essentia::StereoSample *>(Rice::Object);
template<> essentia::StereoSample from_ruby<essentia::StereoSample>(Rice::Object);
template<> Rice::Object to_ruby<essentia::StereoSample>(essentia::StereoSample const &);

//
// vector stuff
//
namespace Rice
{
  namespace detail
  {
     template <typename T>
     struct from_ruby_vector_
     {
        typedef std::vector<T> Retval_T;

        static Retval_T *convert(Rice::Object x);
     };

     template <typename T>
     struct to_ruby_vector_
     {
        static Rice::Object convert(std::vector<T> const &x);
     };

     template <typename T>
     struct from_ruby_vector_vector_
     {
        typedef std::vector<std::vector<T> > Retval_T;

        static Retval_T *convert(Rice::Object x);
     };

     template <typename T>
     struct to_ruby_vector_vector_
     {
        static Rice::Object convert(std::vector<std::vector<T> > const &x);
     };

  } // namespace detail

} // namespace Rice

template <> std::vector<essentia::Real> *from_ruby<std::vector<essentia::Real> *>(Rice::Object);
template <> Rice::Object to_ruby<std::vector<essentia::Real> >(std::vector<essentia::Real> const &);

template <> std::vector<std::complex<essentia::Real> > *from_ruby<std::vector<std::complex<essentia::Real> > *>(Rice::Object);
template <> Rice::Object to_ruby<std::vector<std::complex<essentia::Real> > >(std::vector<std::complex<essentia::Real> > const &);

template <> std::vector<std::string> *from_ruby<std::vector<std::string> *>(Rice::Object);
template <> Rice::Object to_ruby<std::vector<std::string> >(std::vector<std::string> const &);

template <> std::vector<bool> *from_ruby<std::vector<bool> *>(Rice::Object);
template <> Rice::Object to_ruby<std::vector<bool> >(std::vector<bool> const &);

template <> std::vector<int> *from_ruby<std::vector<int> *>(Rice::Object);
template <> Rice::Object to_ruby<std::vector<int> >(std::vector<int> const &);

template <> std::vector<Rice::Essentia::StereoSampleProxy> *from_ruby<std::vector<Rice::Essentia::StereoSampleProxy> *>(Rice::Object);
template <> std::vector<essentia::StereoSample> *from_ruby<std::vector<essentia::StereoSample> *>(Rice::Object);

template <> std::vector<std::vector<essentia::Real> > *from_ruby<std::vector<std::vector<essentia::Real> > *>(Rice::Object);
template <> Rice::Object to_ruby<std::vector<std::vector<essentia::Real> > >(std::vector<std::vector<essentia::Real> > const &);

#endif /* !defined(_RICE_ESSENTIA_TO_FROM_RUBY_HPP_) */
