#include "rice/Data_Type.hpp"
#include "rice/Constructor.hpp"

#include <essentia/iotypewrappers_impl.h>
#include <essentia/streaming/sink.h>
#include <essentia/streaming/source.h>

#include "exception.hpp"
#include "modules.hpp"

namespace Rice
{
  namespace Essentia
  {
    namespace Standard
    {

      /*
       * we need to write wrappers for all the types we need, explicitely
       * naming them and implementing the get()/set() methods
       */
      template <typename T>
      const T &
      wrap_input_get(Rice::Object o)
      {
        Rice::Data_Object<essentia::standard::Input<T> > obj(o);
        const T &result = obj->get();

        return result;
      }

      template <typename T>
      void
      install_inputs(Rice::Data_Type<essentia::standard::Input<T> > &hook, const char name[])
      {
        RUBY_TRY
        {
          hook =
            define_class_under<essentia::standard::Input<T>, essentia::standard::InputBase >(essentia_standard_module(), name)
            .define_constructor(Rice::Constructor<essentia::standard::Input<T> >())
            .template add_handler<essentia::EssentiaException>(handle_essentia_exception)
            .define_method("full_name", &essentia::standard::Input<T>::fullName)
            .define_method("set", &essentia::standard::Input<T>::template set<T>)
            .define_method("get", &wrap_input_get<T>)
            ;
        }
        RUBY_CATCH
      }

      static Rice::Data_Type<essentia::standard::Input<int> > rb_cStandardIntInput;
      static Rice::Data_Type<essentia::standard::Input<essentia::Real> > rb_cStandardRealInput;
      static Rice::Data_Type<essentia::standard::Input<std::vector<essentia::Real> > > rb_cStandardRealVectorInput;
      static Rice::Data_Type<essentia::standard::Input<std::vector<std::vector<essentia::Real> > > > rb_cStandardRealVectorVectorInput;
      static Rice::Data_Type<essentia::standard::Input<std::complex<essentia::Real> > > rb_cStandardComplexInput;
      static Rice::Data_Type<essentia::standard::Input<std::vector<std::complex<essentia::Real> > > > rb_cStandardComplexVectorInput;
      static Rice::Data_Type<essentia::standard::Input<std::string> > rb_cStandardStringInput;
      static Rice::Data_Type<essentia::standard::Input<std::vector<std::string> > > rb_cStandardStringVectorInput;

      void install_io()
      {
        install_inputs<int>(rb_cStandardIntInput, "IntInput");

        install_inputs<essentia::Real>(rb_cStandardRealInput, "RealInput");
        install_inputs<std::vector<essentia::Real> >(rb_cStandardRealVectorInput, "RealVectorInput");
        install_inputs<std::vector<std::vector<essentia::Real> > >(rb_cStandardRealVectorVectorInput, "RealVectorVectorInput");

        install_inputs<std::complex<essentia::Real> >(rb_cStandardComplexInput, "ComplexInput");
        install_inputs<std::vector<std::complex<essentia::Real> > >(rb_cStandardComplexVectorInput, "ComplexVectorInput");

        install_inputs<std::string>(rb_cStandardStringInput, "StringInput");
        install_inputs<std::vector<std::string> >(rb_cStandardStringVectorInput, "StringVectorInput");
      }

    }

    namespace Streaming
    {
      static Rice::Data_Type<essentia::streaming::Sink<std::vector<essentia::Real> > > streaming_real_vector_sink_type;

      static void
      install_real_vector_sinks()
      {
         RUBY_TRY
         {
         }
         RUBY_CATCH
      }

      static Rice::Data_Type<essentia::streaming::Source<std::vector<essentia::Real> > > streaming_real_vector_source_type;

      static void
      install_real_vector_sources()
      {
         RUBY_TRY
         {
         }
         RUBY_CATCH
      }

      void install_io()
      {
        install_real_vector_sinks();
        install_real_vector_sources();
      }
    }

  }

}
