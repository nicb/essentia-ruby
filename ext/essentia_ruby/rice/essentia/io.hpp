#if !defined(_RICE_ESSENTIA_IO_HPP_)
# define _RICE_ESSENTIA_IO_HPP_

namespace Rice
{
  namespace Essentia
  {
    namespace Standard
    {
      void install_io();
    }

    namespace Streaming
    {
      void install_io();
    }

  }
}

#endif /* !defined(_RICE_ESSENTIA_IO_HPP_) */
