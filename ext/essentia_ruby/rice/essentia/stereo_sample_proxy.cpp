#include <iostream>
#include <rice/Constructor.hpp>

#include "exception.hpp"
#include "modules.hpp"
#include "stereo_sample_proxy.hpp"

namespace Rice {
  namespace Essentia {

    essentia::Real
    set_left_value(Rice::Object o, Rice::Object l)
    {
       essentia::StereoSample *s = from_ruby<essentia::StereoSample *>(o);
       return s->first = from_ruby<essentia::Real>(l);
    }

    essentia::Real
    set_right_value(Rice::Object o, Rice::Object r)
    {
       essentia::StereoSample *s = from_ruby<essentia::StereoSample *>(o);
       return s->second = from_ruby<essentia::Real>(r);
    }

    StereoSampleProxy::StereoSampleProxy(std::vector<essentia::Real> *v)
    {
      this->first = this->second = 0.0;

      if (v != 0)
      {
        if (v->size() >= 2)
          this->second = (*v)[1];
        if (v->size() >= 1)
          this->first = (*v)[0];
      }
    }

    static Rice::Data_Type<essentia::StereoSample> rb_cStereoSample;
    static Rice::Data_Type<Rice::Essentia::StereoSampleProxy> rb_cStereoSampleProxy;
    typedef const essentia::Real& (essentia::StereoSample::*const_get)() const;

    void
    install_stereo_sample_proxy()
    {
      RUBY_TRY
      {
        rb_cStereoSample =
          define_class_under<essentia::StereoSample>(essentia_module(), "StereoSample__")
          .add_handler<essentia::EssentiaException>(handle_essentia_exception)
          .define_method("left=", &set_left_value, (Arg("value")))
          .define_method("right=", &set_right_value, (Arg("value")))
          .define_method("left", const_get(&essentia::StereoSample::left))
          .define_method("right", const_get(&essentia::StereoSample::right))
          .define_method("x", const_get(&essentia::StereoSample::x))
          .define_method("y", const_get(&essentia::StereoSample::y))
          ;

        rb_cStereoSampleProxy =
          define_class_under<Rice::Essentia::StereoSampleProxy, essentia::StereoSample>(essentia_module(), "StereoSample")
          .define_constructor(Rice::Constructor<Rice::Essentia::StereoSampleProxy, std::vector<essentia::Real> *>(), (Arg("init") = new std::vector<essentia::Real>()))
          .add_handler<essentia::EssentiaException>(handle_essentia_exception)
          ;
      }
      RUBY_CATCH
    }

  }
}
