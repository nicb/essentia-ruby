#include <iostream>
#include <rice/Constructor.hpp>

#include "exception.hpp"
#include "modules.hpp"

namespace Rice
  {
  namespace Essentia
  {
    static Rice::Data_Type<std::vector<essentia::Real> > rb_cStdVectorEssentiaReal;
    static Rice::Data_Type<std::vector<std::vector<essentia::Real> > > rb_cStdVectorVectorEssentiaReal;

    void
    install_vector_real()
    {
      RUBY_TRY
      {
        rb_cStdVectorEssentiaReal =
          define_class_under<std::vector<essentia::Real> >(essentia_module(), "RealVector")
          .add_handler<essentia::EssentiaException>(handle_essentia_exception)
          ;
      }
      RUBY_CATCH
    }

    void
    install_vector_vector_real()
    {
      RUBY_TRY
      {
        rb_cStdVectorVectorEssentiaReal =
          define_class_under<std::vector<std::vector<essentia::Real> > >(essentia_module(), "RealVectorVector")
          .add_handler<essentia::EssentiaException>(handle_essentia_exception)
          ;
      }
      RUBY_CATCH
    }

  }
}
