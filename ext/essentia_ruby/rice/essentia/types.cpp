#include <iostream>
#include <rice/Constructor.hpp>

#include "exception.hpp"
#include "modules.hpp"
#include "types.hpp"
#include "real_vector.hpp"
#include "complex_vector.hpp"
#include "string_vector.hpp"

namespace Rice {
  namespace Essentia {

    static Rice::Data_Type<essentia::TypeProxy> rb_cTypeProxy;
    typedef void (TypeProxyProxy::*check_type_ptr)(const std::type_info&, const std::type_info&) const;

    static void
    install_type_info()
    {
      RUBY_TRY
      {
        rb_cTypeProxy =
          define_class_under<essentia::TypeProxy>(essentia_module(), "TypeProxy")
          .define_director<TypeProxyProxy>()
          .add_handler<essentia::EssentiaException>(handle_essentia_exception)
          .define_method("name", &TypeProxyProxy::name)
          .define_method("set_name", &TypeProxyProxy::setName)
          .define_method("check_type", check_type_ptr(&TypeProxyProxy::checkType))
          .define_method("check_same_type_as", &TypeProxyProxy::checkSameTypeAs)
          .define_method("check_same_vector_type_as", &TypeProxyProxy::checkVectorSameTypeAs)
#if 0
          /*
           * These following two methods cannot be implemented currently, or
           * at least until I (or somebody) figures out the way to do it
           *
           */
          .define_method("type_info", &TypeProxyProxy::default_typeInfo)
          .define_method("vector_type_info", &TypeProxyProxy::default_vectorTypeInfo)
#endif
          ;
      }
      RUBY_CATCH
    }

    void install_essentia_types()
    {
       void install_stereo_sample_proxy();

       install_vector_real();
       install_vector_vector_real();
       install_vector_complex();
       install_vector_string();
       install_stereo_sample_proxy();
       install_type_info();
    }
  }
}
