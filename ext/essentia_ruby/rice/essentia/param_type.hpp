#if !defined(_RICE_ESSENTIA_PARAM_TYPE_HPP_)
# define _RICE_ESSENTIA_PARAM_TYPE_HPP_

#include <rice/Enum.hpp>
#include <rice/Array.hpp>

#include <essentia/parameter.h>

namespace Rice
{
  namespace Essentia
  {
    inline bool is_it_a(const Rice::Object o, const ruby_value_type t) { return o.rb_type() == t; }
    bool is_it_a_stereo_sample(const Rice::Array);
    bool is_it_an_array_of(const Rice::Array, const ruby_value_type, const ruby_value_type = RUBY_T_NONE);
    bool is_it_an_array_of_stereo_samples(const Rice::Array);
    essentia::Parameter::ParamType to_param_type(const Rice::Object, const ruby_value_type, const essentia::Parameter::ParamType);
  }
} 

#endif /* !defined(_RICE_ESSENTIA_PARAM_TYPE_HPP_) */
