#include <iostream>

#include "rice/Data_Type.hpp"
#include "rice/Data_Object.hpp"
#include "rice/Constructor.hpp"
#include "rice/Hash.hpp"


#include "modules.hpp"
#include "exception.hpp"
#include "parameter_map.hpp"
#include "to_from_ruby.hpp"

namespace Rice
{
  namespace Essentia
  {

    Rice::Object
    ParameterMapProxy::create(Rice::Object o)
    {
      Data_Object<ParameterMapProxy> *obj = new Data_Object<ParameterMapProxy>(new ParameterMapProxy);
      Rice::Essentia::ParameterMapProxy *result = from_ruby<ParameterMapProxy *>(*obj);
    
      if (o.rb_type() == T_HASH)
      {
        Rice::Hash h(o);
    
        Rice::Hash::const_iterator cur = h.begin();
        Rice::Hash::const_iterator end = h.end();
    
        for (; cur != end; ++cur)
          result->add(from_ruby<std::string>(cur->first), from_ruby<essentia::Parameter>(cur->second));
      }

      return *obj;
    }

    static Rice::Data_Type<ParameterMapProxy> rb_cParameterMap;

    void
    install_parameter_maps()
    {
       RUBY_TRY
       {
         rb_cParameterMap =
           define_class_under<ParameterMapProxy>(essentia_module(), "ParameterMap")
           .define_constructor(Constructor<ParameterMapProxy>())
           .add_handler<essentia::EssentiaException>(handle_essentia_exception)
           .define_method("size", &ParameterMapProxy::size_)
           .define_method("add", &ParameterMapProxy::add_)
           .define_method("[]", &ParameterMapProxy::sqb)
           .define_method("empty?", &ParameterMapProxy::is_empty)
           .define_method("keys", &ParameterMapProxy::keys)
           .define_singleton_method("create", &ParameterMapProxy::create)
           .include_module(rb_mEnumerable)
           ;
       }
       RUBY_CATCH
    }
  }
}

#if 0

/*
 * from_ruby:
 * if Rice::Object is a hash, populate the ParameterMapProxy.
 * If it is not, just return an empty object.
 */

template <>
Rice::Essentia::ParameterMapProxy *
from_ruby<Rice::Essentia::ParameterMapProxy *>(Rice::Object o)
{
  Rice::Essentia::ParameterMapProxy *result = new Rice::Essentia::ParameterMapProxy;

  if (o.rb_type() == T_HASH)
  {
    Rice::Hash h(o);

    Rice::Hash::const_iterator cur = h.begin();
    Rice::Hash::const_iterator end = h.end();

    for (; cur != end; ++cur)
    {
std::cout << cur->first << std::endl;
      result->add(from_ruby<std::string>(cur->first), from_ruby<essentia::Parameter>(cur->second));
std::cout << "result: " << result << " size: " << result->size_() << std::endl;
    }
  }

  return result;
}

template <>
Rice::Object
to_ruby<Rice::Essentia::ParameterMapProxy>(Rice::Essentia::ParameterMapProxy const & pm)
{
  Rice::Hash h;

  Rice::Essentia::ParameterMapProxy::iterator b = pm.begin();
  Rice::Essentia::ParameterMapProxy::iterator e = pm.end();

  for (Rice::Essentia::ParameterMapProxy::iterator cur = b; cur != e; ++cur)
  {
    Rice::Hash::Proxy rhp(h, cur->first());
    rhp.operator=<Rice::Essentia::ParameterBase>(cur->second());
  }

  return h;
}
#endif
