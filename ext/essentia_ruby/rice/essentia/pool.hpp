#if !defined(_RICE_ESSENTIA_POOL_HPP_)
# define _RICE_ESSENTIA_POOL_HPP_

namespace Rice
{
  namespace Essentia
  {
    void install_pools();
  }
}

#endif /* !defined(_RICE_ESSENTIA_POOL_HPP_) */
