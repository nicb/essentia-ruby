#if !defined(_RICE_ESSENTIA_COMPLEX_VECTOR_HPP_)
#  define _RICE_ESSENTIA_COMPLEX_VECTOR_HPP_

#include <essentia/types.h>

namespace Rice {
  namespace Essentia {
    void install_vector_complex();
  }
}

#endif /* !defined(_RICE_ESSENTIA_COMPLEX_VECTOR_HPP_) */
