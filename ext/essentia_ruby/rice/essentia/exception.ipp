#include <typeinfo>
#include "rice/Object.hpp"
#include "rice/detail/demangle.hpp"

namespace Rice {
  namespace Essentia {

    template <typename T>
    void
    throw_invalid_from_ruby_argument_exception(Rice::Object x)
    {
      std::string s("Rice::Essentia::from_ruby: unable to convert ");
      s += x.class_of().name().c_str();
      s += " to ";
      s += detail::demangle(typeid(T).name());
      throw std::invalid_argument(s.c_str());
    }

    template <typename T>
    void
    throw_invalid_to_ruby_argument_exception()
    {
      std::string s("Rice::Essentia::to_ruby: unable to convert ");
      s += detail::demangle(typeid(T).name());
      s += " to Rice::Object";
      throw std::invalid_argument(s.c_str());
    }
  }
}

