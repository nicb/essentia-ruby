#if !defined(_RICE_ESSENTIA_STEREO_SAMPLE_HPP_)
#  define _RICE_ESSENTIA_STEREO_SAMPLE_HPP_

#include <essentia/types.h>

/*
 * we cannot include types.hpp here because there's a chicken-and-egg header
 * problem between these two headers. So we include just the wanted prototype
 * instead.
 */
template <> std::vector<essentia::Real> *from_ruby<std::vector<essentia::Real> *>(Rice::Object);

namespace Rice {
  namespace Essentia {

    class StereoSampleProxy : public essentia::StereoSample
    {
    public:
      StereoSampleProxy(std::vector<essentia::Real> *v = 0);
    };
  }
}

#endif /* !defined(_RICE_ESSENTIA_STEREO_SAMPLE_HPP_) */
