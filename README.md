# essentia-ruby

`Essentia` is an Free Software library and tools for audio and music analysis, description and synthesis.

`essentia-ruby` is the `ruby` binding for this library, using the
[`rice`](https://github.com/jasonroelofs/rice) framework.

## Notice

[**PLEASE NOTE**: the whole library is currently (26/1/2018) in the works. As
such, almost *all* `spec`s fail. Don't be surprised if they do. If you wish to
help see [below](./README.md#Contributing)].

## Requirements

In order to link properly the development packages of the following libraries
have to be found during compilation:

* `libsamplerate`
* `libavcodec`
* `libavformat`
* `libavutil`
* `libavresample`
* `libyaml`
* `libfftw3f`
* `libtag` *(optional)*

## Installation

In order to make this work, you have to run the following command--line
commands:

```bash
#
# this loads the submodule repositories
#
$ git submodule update --init 'ext/essentia_ruby/essentia'        # the actual essentia library; this takes quite a long while
$ git submodule update --init 'ext/essentia_ruby/essentia-audio'  # the audio test samples; this takes quite a long while
$ cd ext/essentia_ruby/essentia
$ ./waf configure [--library-type=static] [--build-type=release]
$ ./waf # this takes quite a while
```
After this, you have to compile the `C++` part of the `gem`:

```bash
$ rake compile
```

After this you can run the tests to verify that everything is in place:

```bash
$ rake
```

If everything is ok you should be good to go.

In order to make something with this `gem`, you should:

* add this line to your application's Gemfile:

```ruby
gem 'essentia-ruby'
```

* and then execute:

```bash
    $ bundle
```

Or install it yourself as:

```bash
    $ gem install essentia-ruby
```

Then you can do:

```ruby

require 'essentia'
```
and boum, you have the whole library.

## Helpers

`essentia-ruby` has helpers that can be used as methods or functions to run
the desired analysis algorithm. You are welcome to add more helpers.

## Usage

<!-- TODO: add sample code -->

## Contributing

1. Please read [here](./TODO.md) what the priorities in contributing are 
1. Fork it ( https://github.com/[my-github-username]/essentia-ruby/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
