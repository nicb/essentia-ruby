require 'spec_helper'

class ParTester

  attr_reader :key, :value, :to_out

  def initialize(k, v, o)
    @key = k
    @value = v
    @to_out = o
  end

end

class AlgoTester

  attr_reader :key, :create, :args

  def initialize(k, c, a = [])
    @key = k
    @create = c
    @args = a
  end

  def to_pars
    self.args.map { |arg| [arg.key, arg.value] }.flatten
  end

end

describe Essentia::Standard::AlgorithmFactory do

  before :example do
    @algos =
    [
      AlgoTester.new('AudioLoader', :create1, [ParTester.new("filename", Essentia::Parameter.new_string(File.join(AUDIO_FIXTURES_DIR, "dissonance_sin_44100_mono.wav")), :to_s) ]),
    ]
  end

  it 'cannot be created' do
    expect { Essentia::Standard::AlgorithmFactory.new }.to raise_error(NoMethodError)
  end

  it 'gets always the same instance (it is a singleton)' do
    expect((a = Essentia::Standard::AlgorithmFactory.instance)).not_to be nil
    expect((b = Essentia::Standard::AlgorithmFactory.instance)).not_to be nil
    expect(a === b).to be true
  end

  it 'has a keys() method which will return all available algorithms' do
    expect(Essentia::Standard::AlgorithmFactory.respond_to?(:keys)).to be true
    expect(Essentia::Standard::AlgorithmFactory.keys.size).to be > 10
  end

  it 'can create all algorithms available (default creation)' do
    expect((algos = Essentia::Standard::AlgorithmFactory.keys).empty?).to be false
    algos.each do
      |aname|
      expect((a = Essentia::Standard::AlgorithmFactory.create0(aname))).not_to(be(nil), aname)
      expect(a.class).to be Essentia::Standard::Algorithm
    end
  end

  it 'can create algorithms with parameters' do
    @algos.each do
      |algo|
byebug
      expect((a = Essentia::Standard::AlgorithmFactory.send(algo.create, algo.key, *algo.to_pars))).not_to(be(nil), algo.key)
      expect(a.inputs.is_a?(Hash)).to be true
#     expect(a.outputs.is_a?(Hash)).to be true
      algo.args.each { |arg| expect(a.parameter(arg.key).send(arg.to_out)).to(eq(arg.value.send(arg.to_out)), arg.key) }
    end
  end

end

describe Essentia::Streaming::AlgorithmFactory do

  it 'cannot be created' do
    expect { Essentia::Streaming::AlgorithmFactory.new }.to raise_error(NoMethodError)
  end

  it 'gets always the same instance (it is a singleton)' do
    expect((a = Essentia::Streaming::AlgorithmFactory.instance)).not_to be nil
    expect((b = Essentia::Streaming::AlgorithmFactory.instance)).not_to be nil
    expect(a === b).to be true
  end

  it 'has a keys() method which will return all available algorithms' do
    expect(Essentia::Streaming::AlgorithmFactory.respond_to?(:keys)).to be true
    expect(Essentia::Streaming::AlgorithmFactory.keys.size).to be > 10
  end

  it 'can create all algorithms available' do
    expect((algos = Essentia::Streaming::AlgorithmFactory.keys).empty?).to be false
    algos.each do
      |aname|
      expect((a = Essentia::Streaming::AlgorithmFactory.create0(aname))).not_to(be(nil), aname)
      expect(a.class).to be Essentia::Streaming::Algorithm
    end
  end

end
