require 'spec_helper'

describe Essentia::ParamType do

  before :example do
    @constants =
      %w(
        UNDEFINED
        REAL
        STRING
        BOOL
        INT
        STEREOSAMPLE
        VECTOR_REAL
        VECTOR_STRING
        VECTOR_BOOL
        VECTOR_INT
        VECTOR_STEREOSAMPLE
        VECTOR_VECTOR_REAL
        VECTOR_VECTOR_STRING
        VECTOR_VECTOR_STEREOSAMPLE
        VECTOR_MATRIX_REAL
        MAP_VECTOR_REAL
        MAP_VECTOR_STRING
        MAP_VECTOR_INT
        MAP_REAL
        MATRIX_REAL
      )
  end

  it 'has constants that can be checked' do
    @constants.each do
      |c|
      expect(Essentia::ParamType.constants.include?(c.to_sym)).to(be(true), c)
    end
  end

end

def check_methods(obj)

  parameter_methods =
  [
    :clear, :assign, :==, :!=, :type, :configured?, :to_s, :downcase,
    :to_boolean, :to_double, :to_f, :to_stereo_sample, :to_i, :to_real,
    :to_vector_real, :to_vector_string, :to_vector_int, :to_vector_boolean,
    :to_vector_stereo_sample, :to_vector_vector_real, :to_vector_vector_string,
    :to_vector_vector_stereo_sample, :to_vector_matrix_real, :to_map_vector_real,
    :to_map_vector_string, :to_map_real, :to_matrix_real,
  ]

  parameter_methods.each do
    |m|
    expect(obj.respond_to?(m)).to(be(true), m.to_s)
  end

end

def check_false_positives(obj, excluded)

  to_methods =
  [
    :to_boolean, :to_double, :to_f, :to_stereo_sample, :to_i, :to_real,
    :to_vector_real, :to_vector_string, :to_vector_int, :to_vector_boolean,
    :to_vector_stereo_sample, :to_vector_vector_real, :to_vector_vector_string,
    :to_vector_vector_stereo_sample, :to_vector_matrix_real, :to_map_vector_real,
    :to_map_vector_string, :to_map_real, :to_matrix_real,
  ]
  parameter_methods = to_methods - excluded

  parameter_methods.each do
    |m|
    expect { obj.send(m) }.to(raise_error(RuntimeError), "class: #{obj.class.name}, method: #{m.to_s}")
  end

end

describe 'Essentia::Parameter (simple, unconfigured)' do

  it 'can be created unconfigured' do
    expect(p = Essentia::Parameter.new(Essentia::ParamType::UNDEFINED)).not_to be nil
  end

  it 'has all the methods in place' do
    expect((p = Essentia::Parameter.new(Essentia::ParamType::UNDEFINED))).not_to be nil
    check_methods(p)
  end

  it 'ought to be *unconfigured*' do
    expect((p = Essentia::Parameter.new(Essentia::ParamType::UNDEFINED))).not_to be nil
    expect(p.configured?).to be false
  end

end

describe 'Essentia::Parameter (string, configured)' do

  before :example do
    @parameter_value = "TEST test"
    @p = Essentia::Parameter.new_string(@parameter_value)
    @not_p = Essentia::Parameter.new_string("not Test")
  end

  it 'cannot be created witout a string' do
    expect(@p).not_to be nil
    expect(@not_p).not_to be nil
    expect { Essentia::Parameter.new_string() }.to raise_error(ArgumentError)
  end

  it 'cannot be created with a wrong argument (not a string)' do
    [ 42, 23.23, true, [1.0, -1.0], [0, 1, 2, 3, 4], [0.0, 0.1, 0.2] ].each do
      |warg|
      expect { Essentia::Parameter.new_string(warg) }.to raise_error(TypeError), warg.to_s
    end
  end

  it 'has all the methods in place (String)' do
    check_methods(@p)
  end

  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::STRING)
    expect(@p.to_s).to eq(@parameter_value)
    expect(@p.downcase).to eq(@parameter_value.downcase)
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    check_false_positives(@p, [])
  end

end

describe 'Essentia::Parameter (Essentia::Real)' do

  before :example do
    @parameter_value = 23.23
    @p = Essentia::Parameter.new_real(@parameter_value)
    @not_p = Essentia::Parameter.new_real(@parameter_value / 2.0)
    @wanted_precision = 20
  end

  it 'cannot be created witout a real' do
    expect(@p).not_to be nil
    expect(@not_p).not_to be nil
    expect { Essentia::Parameter.new_real() }.to raise_error(ArgumentError)
  end

  it 'cannot be created with a wrong argument (not a real)' do
    [ 'test', true, [1.0, -1.0], [0, 1, 2, 3, 4], [0.0, 0.1, 0.2] ].each do
      |warg|
      expect { Essentia::Parameter.new_real(warg) }.to raise_error(TypeError), warg.to_s
    end
  end

  it 'has all the methods in place (Real)' do
    check_methods(@p)
  end

  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::REAL)
    expect(@p.to_real).to be_within(1.0e-5).of(@parameter_value)
    expect(@p.to_s.size).to eq(13)
    expect(@p.to_s(@wanted_precision).size).to eq(@wanted_precision+1)
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    expect{ @p.to_stereo_sample }.to raise_error(RuntimeError)
    check_false_positives(@p, [:to_i, :to_f, :to_real, :to_double])
  end

end

describe 'Essentia::Parameter (bool)' do

  before :example do
    @parameter_value = true
    @p = Essentia::Parameter.new_bool(@parameter_value)
    @not_p = Essentia::Parameter.new_bool(!@parameter_value)
  end

  it 'cannot be created witout a bool' do
    expect(@p).not_to be nil
    expect(@not_p).not_to be nil
    expect { Essentia::Parameter.new_bool() }.to raise_error(ArgumentError)
  end

  #
  # no arguments are ever typed-out errors with bool
  # 

  it 'has all the methods in place (Bool)' do
    check_methods(@p)
  end

  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::BOOL)
    expect(@p.to_boolean).to be true
    expect(@not_p.to_boolean).to be false
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    check_false_positives(@p, [:to_boolean])
  end

end

describe 'Essentia::Parameter (int)' do

  before :example do
    @parameter_value = 23.to_i
    @p = Essentia::Parameter.new_int(@parameter_value)
    @not_p = Essentia::Parameter.new_int(@parameter_value / 2)
  end

  it 'cannot be created with a wrong argument (not an int)' do
    [ 'test', true, [1.0, -1.0], [0, 1, 2, 3, 4], [0.0, 0.1, 0.2] ].each do
      |warg|
      expect { Essentia::Parameter.new_int(warg) }.to raise_error(TypeError), warg.to_s
    end
  end

  it 'has all the methods in place (Int)' do
    check_methods(@p)
  end

  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::INT)
    expect(@p.to_i).to eq(@parameter_value)
    expect(@p.to_s).to eq(@parameter_value.to_s)
    expect(@p.to_s.size).to eq(@parameter_value.to_s.size)
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    check_false_positives(@p, [:to_i, :to_f, :to_real])
  end

end

describe 'Essentia::Parameter (uint)' do

  #
  # over 2**24 we see strange aberrations in numbers
  #
  before :example do
    @parameter_value = ((2**24)-1)
    @p = Essentia::Parameter.new_uint(@parameter_value)
    @not_p = Essentia::Parameter.new_uint(@parameter_value / 2)
  end

  it 'cannot be created with a wrong argument (not a uint)' do
    [ 'test', true, [1.0, -1.0], [0, 1, 2, 3, 4], [0.0, 0.1, 0.2] ].each do
      |warg|
      expect { Essentia::Parameter.new_uint(warg) }.to raise_error(TypeError), warg.to_s
    end
  end

  it 'has all the methods in place (Uint)' do
    check_methods(@p)
  end

  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::INT)
    expect(@p.to_i).to eq(@parameter_value)
    expect(@p.to_s(10)).to eq(@parameter_value.to_s[0..9])
    expect(@p.to_s.size).to eq(@parameter_value.to_s.size)
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    check_false_positives(@p, [:to_i, :to_f, :to_real])
  end

end

describe 'Essentia::Parameter (essentia::StereoSample)' do

  before :example do
    @parameter_value = [0.999, -0.999]
    @ss = Essentia::StereoSample.new
    @not_ss = Essentia::StereoSample.new
    @ss.left = @parameter_value[0]
    @ss.right = @parameter_value[1]
    @not_ss.left = @parameter_value[0]/2.0
    @not_ss.right = @parameter_value[1]/2.0
    @p = Essentia::Parameter.new_stereo_sample(@ss)
    @not_p = Essentia::Parameter.new_stereo_sample(@not_ss)
  end

  it 'has all the methods in place (StereoSample)' do
    check_methods(@p)
  end

  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::STEREOSAMPLE)
    a = @p.to_stereo_sample
    expect(a[0]).to be_within(1e-6).of(@ss.left)
    expect(a[1]).to be_within(1e-6).of(@ss.right)
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    check_false_positives(@p, [:to_stereo_sample])
  end

end

describe 'Essentia::Parameter (vector real)' do

  before :example do
    @parameter_value = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0]
    @p = Essentia::Parameter.new_vector_real(@parameter_value)
    @not_p = Essentia::Parameter.new_vector_real(@parameter_value.map { |x| x/2.0 })
  end

  it 'cannot be created with a wrong argument (not a vector real)' do
    [ 'test', true, 23.23, -1, -(2**12).to_i, ].each do
      |warg|
      expect { Essentia::Parameter.new_vector_real(warg) }.to raise_error(TypeError), warg.to_s
    end
  end
  
  it 'has all the methods in place (VectorReal)' do
    check_methods(@p)
  end
  
  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::VECTOR_REAL)
    expect(@p.respond_to?(:to_vector_real)).to be true
    a = @p.to_vector_real
    a.each_index { |idx| expect(a[idx]).to be_within(1e-12).of(@parameter_value[idx]) }
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    check_false_positives(@p, [:to_vector_real])
  end
end

describe 'Essentia::Parameter (std::vector<std::string>)' do

  before :example do
    @parameter_value = %w(one two three four five)
    @p = Essentia::Parameter.new_vector_string(@parameter_value)
    @not_p = Essentia::Parameter.new_vector_string(@parameter_value.map { |x| x + ' plus one' })
  end
  
  it 'cannot be created with a wrong argument (not a vector string)' do
    [ 'test', true, 23.23, -1, -(2**12).to_i, [0, 1.1, 2.1, 3, 4] ].each do
      |warg|
      expect { Essentia::Parameter.new_vector_string(warg) }.to raise_error(TypeError), warg.to_s
    end
  end
  
  it 'has all the methods in place (VectorString)' do
    check_methods(@p)
  end
  
  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::VECTOR_STRING)
    a = @p.to_vector_string
    a.each_index { |idx| expect(a[idx]).to(eq(@parameter_value[idx]), a[idx]) }
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    check_false_positives(@p, [:to_vector_string])
  end
end

describe 'Essentia::Parameter (std::vector<bool>)' do

  before :example do
    @parameter_value = [true, false, false, true]
    @p = Essentia::Parameter.new_vector_bool(@parameter_value)
    @not_p = Essentia::Parameter.new_vector_bool(@parameter_value.map { |x| !x })
  end

  it 'cannot be created with a wrong argument (not a vector bool)' do
    [ 'test', true, 23.23, -1, -(2**12).to_i,  ].each do
      |warg|
      expect { Essentia::Parameter.new_vector_bool(warg) }.to raise_error(TypeError), warg.to_s
    end
  end

  it 'has all the methods in place (VectorBool)' do
    check_methods(@p)
  end

  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::VECTOR_BOOL)
    a = @p.to_vector_boolean
    a.each_index { |idx| expect(a[idx]).to(be(@parameter_value[idx]), a[idx].to_s) }
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    check_false_positives(@p, [:to_vector_boolean])
  end

end

describe 'Essentia::Parameter (std::vector<int>)' do

  before :example do
    @parameter_value = [-((2**24)-1), 0, (2**24)-1]
    @p = Essentia::Parameter.new_vector_int(@parameter_value)
    @not_p = Essentia::Parameter.new_vector_int(@parameter_value.map { |x| x / 2 })
  end

  it 'cannot be created with a wrong argument (not a vector int)' do
    [ 'test', true, 23.23, -1, -(2**12).to_i, %w(1 2 3 4)  ].each do
      |warg|
      expect { Essentia::Parameter.new_vector_int(warg) }.to raise_error(TypeError), warg.to_s
    end
  end

  it 'has all the methods in place (VectorInt)' do
    check_methods(@p)
  end

  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::VECTOR_INT)
    a = @p.to_vector_int
    a.each_index { |idx| expect(a[idx]).to(be(@parameter_value[idx]), a[idx].to_s) }
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    check_false_positives(@p, [:to_vector_int])
  end

end

describe 'Essentia::Parameter (std::vector<essentia::StereoSample>)' do

  before :example do
    @parameter_value = [ Essentia::StereoSample.new ] * 100
    @p = Essentia::Parameter.new_vector_stereo_sample(@parameter_value)
    @not_p = Essentia::Parameter.new_vector_stereo_sample(@parameter_value * 2)
  end

  it 'has all the methods in place (VectorStereoSample)' do
    check_methods(@p)
  end

  it 'actually does work' do
    expect(@p.type).to eq(Essentia::ParamType::VECTOR_STEREOSAMPLE)
    a = @p.to_vector_stereo_sample
    a.each_index do
      |idx|
      expect(a[idx][0]).to be_within(1e-8).of(@parameter_value[idx].left)
      expect(a[idx][1]).to be_within(1e-8).of(@parameter_value[idx].right)
    end
    expect(@p == @p).to be true
    expect(@p == @not_p).to be false
    expect(@p != @not_p).to be true
    expect(@p.configured?).to be true
    check_false_positives(@p, [:to_vector_stereo_sample])
  end

end
