require 'spec_helper'

describe Essentia::Standard::Algorithm do

  before :example do
    @a = Essentia::Standard::Algorithm.new
  end

  it 'can be created' do
    expect(@a).not_to be nil
  end

  meths = [:reset, :compute, :input_names, :output_names, :input_types, :output_types,
   :declare_parameters, :set_parameters, :configure=, :configure, :default_parameters,
   :parameter]

  meths.each do
    |m|
    it "responds to a \"#{m.to_s}\" method" do
      expect(@a.respond_to?(m)).to be(true), m.to_s
    end
  end

end

describe Essentia::Streaming::Algorithm do

  before :example do
    @a = Essentia::Streaming::Algorithm.new
  end

  it 'can be created' do
    expect(@a).not_to be nil
  end

  meths = [:reset, :input_names, :output_names,
   :should_stop, :should_stop?, :acquire_data, :release_data, :disconnect_all, :process,
   :declare_parameters, :set_parameters, :configure=, :configure, :default_parameters,
   :parameter, ] # :synchronize_input, :synchronize_output, :synchronize_io]

  meths.each do
    |m|
    it "responds to a \"#{m.to_s}\" method" do
      expect(@a.respond_to?(m)).to be(true), m.to_s
    end
  end

end
