require 'spec_helper'

class PMTest
  attr_reader :key, :value, :ctor, :out

  def initialize(k, v, c, o)
    @key = k
    @value = v
    @ctor = c
    @out = o
  end

  def to_output
    self.value.send(self.out)
  end

end

class PMTVs < Array

  def to_h
    result = {}
    self.each do
      |v|
      result[v.key] = Essentia::Parameter.send(v.ctor, v.value)
    end
    result
  end

end

describe Essentia::ParameterMap do

  before :example do
    @tvs = PMTVs.new(
      [
        PMTest.new('string', 'test value', :new_string, :to_s),
        PMTest.new('long vector string',['test value'] * 1000, :new_vector_string, :to_vector_string),
        PMTest.new('short vector string',['test value'] * 2, :new_vector_string, :to_vector_string),
        PMTest.new('real',23.23, :new_real, :to_f),
        PMTest.new('long vector real',[23.23] * 1000, :new_vector_real, :to_vector_real),
        PMTest.new('short vector real',[23.23] * 3, :new_vector_real, :to_vector_real),
        PMTest.new('empty vector real',[], :new_vector_real, :to_vector_real),
        PMTest.new('stereo sample',[-1.0, 1.0], :new_stereo_sample, :to_stereo_sample),
        PMTest.new('long vector stereo sample',[[-1.0, 1.0]] * 1000, :new_vector_stereo_sample, :to_vector_stereo_sample),
        PMTest.new('short vector stereo sample',[[-1.0, 1.0]] * 2, :new_vector_stereo_sample, :to_vector_stereo_sample),
        PMTest.new('int', 42, :new_int, :to_i),
        PMTest.new('long vector int',[42] * 1000, :new_vector_int, :to_vector_int),
        PMTest.new('short vector int',[42] * 3, :new_vector_int, :to_vector_int),
        PMTest.new('boolean',true, :new_bool, :to_boolean),
        PMTest.new('long vector boolean',[true, false, true] * 500, :new_vector_bool, :to_vector_boolean),
        PMTest.new('short vector boolean',[true, false, true], :new_vector_bool, :to_vector_boolean),
      ]
    )
    @pm = Essentia::ParameterMap.new
  end

  it 'can be created without parameters' do
    expect((pm = Essentia::ParameterMap.new)).not_to be nil
    expect(pm.size).to eq(0)
    expect(pm.empty?).to be true
  end

  it 'can be created with an empty ruby hash' do
    expect((pm = Essentia::ParameterMap.create({}))).not_to be nil
    expect(pm.size).to eq(0)
    expect(pm.empty?).to be true
  end

  it 'can be created with a filled ruby hash' do
    expect((pm = Essentia::ParameterMap.create(@tvs.to_h))).not_to be nil
    expect(pm.size).to eq(@tvs.to_h.size)
    expect(pm.empty?).to be false
  end

  it 'can be created with a filled ruby hash of parameters' do
    expect((pm = Essentia::ParameterMap.create(@tvs.to_h))).not_to be nil
    expect(pm.size).to eq(@tvs.to_h.size)
    expect(pm.empty?).to be false
  end

  methods = [:add, :[], :each_entry, :map, :size, :keys]

  methods.each do
    |m|
    it "does have a #{m.to_s} method" do
      expect(@pm.respond_to?(m)).to be true
    end
  end

  it 'has a method add() that actually works' do
    expect((pm = Essentia::ParameterMap.new)).not_to be nil
    @tvs.to_h.each { |k, v| pm.add(k, v) }
    expect(pm.size).to eq(@tvs.to_h.size)
  end

  it 'has a method []() that actually works' do
    expect((pm = Essentia::ParameterMap.create(@tvs.to_h))).not_to be nil
    @tvs.each do
      |tpm|
      k = tpm.key
      p = pm[k]
      expect(p.class).to be Essentia::Parameter
      if tpm.out == :to_f
        expect(p.send(tpm.out)).to be_within(1e-6).of(tpm.value)
      elsif tpm.out == :to_vector_real
        # skip testing, too complicated (for now)
      else
        expect(p.send(tpm.out)).to eq (tpm.value)
      end
    end
  end

  it 'can be created with a Hash' do
    expect((pm = Essentia::ParameterMap.create(@tvs.to_h))).not_to be nil
    expect(pm.size).to eq(@tvs.to_h.size)
    @tvs.to_h.each do
      |k, v|
      p = pm[k]
      expect(p).to eq (v)
    end
  end

end
