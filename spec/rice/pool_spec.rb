require 'spec_helper'

def check_methods(obj)

  parameter_methods =
  [
    :add_real, :descriptor_names,
  ]

  parameter_methods.each do
    |m|
    expect(obj.respond_to?(m)).to(be(true), m.to_s)
  end

end

def check_false_positives(obj, excluded)

  to_methods =
  [
  ]
  parameter_methods = to_methods - excluded

  parameter_methods.each do
    |m|
    expect { obj.send(m) }.to(raise_error(RuntimeError), "class: #{obj.class.name}, method: #{m.to_s}")
  end

end

describe Essentia::Pool do

  it 'can be created unconfigured' do
    expect(p = Essentia::Pool.new()).not_to be nil
  end

  it 'has all the methods in place' do
    expect((p = Essentia::Pool.new())).not_to be nil
    check_methods(p)
  end

  it 'has a descriptor_names method that works' do
    r = { :key => 'Real', :value => 23.23 }
    expect((p = Essentia::Pool.new())).not_to be nil
    p.add_real(r[:key], r[:value])
    expect(p.descriptor_names).to eq([r[:key]])
  end

  it 'has an add_real method that works' do
    r = { :key => 'Real', :value => 23.23 }
    wrong_value = 'not a real'
    expect((p = Essentia::Pool.new())).not_to be nil
    p.add_real(r[:key], r[:value])
byebug
#   expect(p.contains_real(r[:key])).to be true
#   expect { p.add_real(r[:key], wrong_value) }.to raise_error(TypeError)
#   expect { p.add_real(r[:key], wrong_value, true) }.to raise_error(TypeError)
    expect(p.real_value(r[:key])).to eq(r[:value])
  end

end
