require 'spec_helper'

describe Essentia::TypeProxy do

  it 'cannot be created (it is a pure virtual)' do
    expect{ Essentia::TypeProxy.new }.to raise_error(TypeError)
  end

end

describe Essentia::StereoSample do

  before :example do
    (@l, @r) = [23.23, -23.23]
    @arg = [@l, @r]
  end

  it 'can be created without a value' do
    expect((x = Essentia::StereoSample.new)).not_to be nil
    expect(x.class).to be Essentia::StereoSample
    expect(x.left).to be_within(1e-8).of(0.0)
    expect(x.right).to be_within(1e-8).of(0.0)
  end

  it 'can be created with an array of two values' do
    expect((x = Essentia::StereoSample.new(@arg))).not_to be nil
    expect(x.class).to be Essentia::StereoSample
    expect(x.left).to be_within(1e-6).of(@arg[0])
    expect(x.right).to be_within(1e-6).of(@arg[1])
  end

  it 'can be created with an empty array' do
    expect((x = Essentia::StereoSample.new([]))).not_to be nil
    expect(x.class).to be Essentia::StereoSample
    expect(x.left).to be_within(1e-8).of(0.0)
    expect(x.right).to be_within(1e-8).of(0.0)
  end

  ss_methods = [:left, :right, :x, :y, :left=, :right=]

  ss_methods.each do
    |m|
    it "has the :#{m} method" do
      expect((x = Essentia::StereoSample.new)).not_to be nil
      expect(x.respond_to?(m)).to(be(true), m.to_s)
    end
  end

  it 'can set up left and right values (separately)' do
    expect((x = Essentia::StereoSample.new)).not_to be nil
    expect(x.left = @l).to be_within(1e-6).of(@l)
    expect(x.right = @r).to be_within(1e-6).of(@r)
    expect(x.left).to be_within(1e-6).of(@l)
    expect(x.right).to be_within(1e-6).of(@r)
  end

end
