require 'spec_helper'

RSpec.shared_examples 'an input' do

  describe '' do
  
    before :example do
      @rvi = klass.new
      @name = "whatever"
      @rvi.set_name(@name)
    end
  
    it 'can be created' do
      expect(@rvi).not_to be nil
    end
  
    it 'has the proper parenting scheme' do
      expect(@rvi.class.superclass).to eq(Essentia::Standard::InputBase)
      expect(@rvi.class.superclass.superclass).to eq(Essentia::TypeProxy)
    end
  
    methods = [:set_name, :name, :full_name, :set, :get]
  
    methods.each do
      |m|
      it "does have a #{m.to_s} method" do
        expect(@rvi.respond_to?(m)).to(be(true), m.to_s)
      end
    end
  
    it 'has a method set_name() and a method name() that actually work' do
      another_rvi = klass.new
      name = "test"
      another_rvi.set_name(name)
      expect(another_rvi.name).to eq(name)
      expect(another_rvi.full_name).to eq("<NoParent>::" + name)
    end
  
    it 'has a method full_name() that actually works' do
      expect(@rvi.full_name()).to eq("<NoParent>::" + @name)
    end
  
  end

end

describe Essentia::Standard::IntInput do

  it_behaves_like 'an input' do
    let(:klass) { Essentia::Standard::IntInput }
  end

  it 'has a method set() and a method get() that actually work' do
    a = 23
    another_ri = Essentia::Standard::IntInput.new
    expect(another_ri.set(a)).to be nil
    expect(another_ri.get).to eq(a)
  end

end

describe Essentia::Standard::RealInput do

  it_behaves_like 'an input' do
    let(:klass) { Essentia::Standard::RealInput }
  end

  it 'has a method set() and a method get() that actually work' do
    a = 23.23
    another_ri = Essentia::Standard::RealInput.new
    expect(another_ri.set(a)).to be nil
    expect(another_ri.get).to be_within(1e-6).of(a)
  end

end

describe Essentia::Standard::RealVectorInput do

  it_behaves_like 'an input' do
    let(:klass) { Essentia::Standard::RealVectorInput }
  end

  it 'has a method set() and a method get() that actually work' do
    a = [1.23, 2.23, 3.23, 4.23]
    another_rvi = Essentia::Standard::RealVectorInput.new
    expect(another_rvi.set(a)).to be nil
    another_rvi.get.each_index do
      |idx|
      expect(another_rvi.get[idx]).to be_within(1e-6).of(a[idx])
    end
  end

end

describe Essentia::Standard::RealVectorVectorInput do

  it_behaves_like 'an input' do
    let(:klass) { Essentia::Standard::RealVectorVectorInput }
  end

  it 'has a method set() and a method get() that actually work' do
    a = [[1.23] * 3, [2.23] * 4, [3.23] * 5, [4.23] * 6]
    another_rvvi = Essentia::Standard::RealVectorVectorInput.new
    expect(another_rvvi.set(a)).to be nil
    another_rvvi.get.each_index do
      |idx|
      arvvi = another_rvvi.get[idx]
      orig = a[idx]
      arvvi.each_index { |jdx| expect(arvvi[jdx]).to be_within(1e-6).of(orig[jdx]) }
    end
  end

end

describe Essentia::Standard::ComplexInput do

  it_behaves_like 'an input' do
    let(:klass) { Essentia::Standard::ComplexInput }
  end

  it 'has a method set() and a method get() that actually work' do
    a = Complex(23.23, 46.46)
    another_ci = Essentia::Standard::ComplexInput.new
    expect(another_ci.set(a)).to be nil
    expect(another_ci.get.real).to be_within(1e-6).of(a.real)
    expect(another_ci.get.imag).to be_within(1e-6).of(a.imag)
  end

end

describe Essentia::Standard::ComplexVectorInput do

  it_behaves_like 'an input' do
    let(:klass) { Essentia::Standard::ComplexVectorInput }
  end

  it 'has a method set() and a method get() that actually work' do
    a = [Complex(1.23, 5.23), Complex(2.23, 6.28), Complex(3.23, 6.28*1.5), Complex(4.23, 6.28*4.23) ]
    another_cvi = Essentia::Standard::ComplexVectorInput.new
    expect(another_cvi.set(a)).to be nil
    another_cvi.get.each_index do
      |idx|
      expect(another_cvi.get[idx].real).to be_within(1e-6).of(a[idx].real)
      expect(another_cvi.get[idx].imag).to be_within(1e-6).of(a[idx].imag)
    end
  end

end

describe Essentia::Standard::StringVectorInput do

  it_behaves_like 'an input' do
    let(:klass) { Essentia::Standard::StringVectorInput }
  end

  it 'has a method set() and a method get() that actually work' do
    a = %w(this is a test)
    another_rvi = Essentia::Standard::StringVectorInput.new
    expect(another_rvi.set(a)).to be nil
    another_rvi.get.each_index do
      |idx|
      expect(another_rvi.get[idx]).to eq(a[idx])
    end
  end

end

describe Essentia::Standard::StringInput do

  it_behaves_like 'an input' do
    let(:klass) { Essentia::Standard::StringInput }
  end

  it 'has a method set() and a method get() that actually work' do
    a = 'this is a test'
    another_rvi = Essentia::Standard::StringInput.new
    expect(another_rvi.set(a)).to be nil
    expect(another_rvi.get).to eq(a)
  end

end
