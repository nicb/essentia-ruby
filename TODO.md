# essentia-ruby TODO

## DONE

* Streaming::io_base (SourceBase, SinkBase)
* spec for Streaming::io_base
* spec for Standard::io_base
* complete methods for InputBase and OutputBase (those that were in Input and
  Output)
* complete methods for specialized Input, Output, Sink, Source
* complete specs for specialized Input, Output, Sink, Source
* Essentia::Parameter
  * create ParameterBase
  * create all ParameterType simple constructors
  * create all ParameterType complex constructors (?)
* spec for Essentia::Parameter
  * create ParameterBase specs
  * create all basic simple ParameterType constructors specs
  * create all full functionality simple ParameterType constructors specs
  * create all full functionality complex ParameterType constructors specs
  * create false positive specs for each Parameter type 
* Essentia::ParameterMap
* spec for Essentia::ParameterMap

## TODO

* complete Essentia::Parameter converters (with specs)
* Essentia::Pool
* specs for Essentia::Pool
* rework all the input/output section (need to do all the I/O specializations)
* correct the Essentia::ParameterMap specs
* Essentia::AlgorithmFactory
* spec for Essentia::AlgorithmFactory
* run full Rice::specs
* travis 
